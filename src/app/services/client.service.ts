import { Injectable } from '@angular/core';
import { AngularFirestore , AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Client } from '../models/Client';
import { FlashMessagesModule } from 'angular2-flash-messages';

@Injectable()
export class ClientService {
  // clientsCollection: AngularFirestoreCollection<Client>;
  clientDoc: AngularFirestoreDocument<Client>;
  clients: Observable<Client[]>;
  client: Observable<Client>;

  constructor(private afs: AngularFirestore) {
    // this.clientsCollection = this.afs.collection('clients', ref => ref.orderBy('lastName', 'asc'));
    // this.clientsCollection = this.getLatestNgFireCollection();
  }

  getClients(): Observable<Client[]> {
    // get clients with their id
    // this.clientsCollection = this.getLatestNgFireCollection();

    return this.getLatestNgFireCollection().snapshotChanges().map(changes => {
      const someval = changes.map(action => {
        const data = action.payload.doc.data() as Client;
        data.id = action.payload.doc.id;
        return data;
      });
      return someval;
    });

  }

  addClient(client: Client) {
    this.getLatestNgFireCollection().add(client);
  }

  getClient(id: string): Observable<Client> {
    this.clientDoc = this.getSpecificClientUsingIdFromFireStore(id);

    this.client = this.clientDoc.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as Client;
        data.id = action.payload.id;
        return data;
      }
    });

    return this.client;
  }

  updateClient(updatedClient: Client) {
    this.clientDoc = this.getSpecificClientUsingIdFromFireStore(updatedClient.id);
    this.clientDoc.update(updatedClient);
  }

  deleteClient(deletedClient: Client) {
    this.clientDoc = this.getSpecificClientUsingIdFromFireStore(deletedClient.id);
    this.clientDoc.delete();
  }

  getLatestNgFireCollection() {
    return this.afs.collection('clients', ref => ref.orderBy('lastName', 'asc'));
  }

  getSpecificClientUsingIdFromFireStore(id: string) {
    return this.afs.doc(`clients/${id}`);
  }

}
