import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

import { ClientService } from '../../services/client.service';
import { Client } from '../../models/Client';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {
  id: string;
  client: Client;
  hasBalance = false;
  showBalanceUpdateInput = false;

  constructor(
    private clientService: ClientService
    , private router: Router
    , private route: ActivatedRoute
    , private flashMsgService: FlashMessagesService
  ) { }

  ngOnInit() {
    // get the id and load details for that client
    this.id = this.route.snapshot.params['id'];
    // get client
    this.clientService.getClient(this.id).subscribe(resultClient => {
      if (resultClient !== null) {
         if (resultClient.balance > 0) {
           this.hasBalance = true;
         }
      }

      this.client = resultClient;
    });
  }

  updateBalance() {
    this.clientService.updateClient(this.client);
    this.flashMsgService.show('Balance Upddated..ta..da', {
      cssClass: 'alert-success', timeout: 4000
    });

  }

  onDeleteClick() {
    if (confirm('Are you sure?')) {
      this.clientService.deleteClient(this.client);

      this.flashMsgService.show('Client Removed', {
        cssClass: 'alert-success', timeout: 4000
      });

      this.router.navigate(['/']);
    }
  }
}
