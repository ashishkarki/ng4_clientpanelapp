export const environment = {
  production: true,
  firebase : {
    apiKey: 'AIzaSyBer0j2uakB1DFJdg32gRUY5l8EyK-SAM4',
    authDomain: 'clientpanelapp-udemy-38129.firebaseapp.com',
    databaseURL: 'https://clientpanelapp-udemy-38129.firebaseio.com',
    projectId: 'clientpanelapp-udemy-38129',
    storageBucket: 'clientpanelapp-udemy-38129.appspot.com',
    messagingSenderId: '344112339511'
  }
};
