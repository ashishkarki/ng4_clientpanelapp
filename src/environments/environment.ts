// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: 'AIzaSyBer0j2uakB1DFJdg32gRUY5l8EyK-SAM4',
    authDomain: 'clientpanelapp-udemy-38129.firebaseapp.com',
    databaseURL: 'https://clientpanelapp-udemy-38129.firebaseio.com',
    projectId: 'clientpanelapp-udemy-38129',
    storageBucket: 'clientpanelapp-udemy-38129.appspot.com',
    messagingSenderId: '344112339511'
  }
};
